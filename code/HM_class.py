import pandas as pd
import numpy as np
import os

# res_path = os.getcwd()
# path = res_path+r'\data\raw_HM_data.csv'


class HM_Matrix():
    """
    Description: Class for processing computing head tracking data gathered from Mono360
    Database state: https://gitlab.com/gpds-unb/mono360/-/commit/4d7e954a0969b1d592b70ccc0a8313cbcedd9953
    """
    def __init__(self, path, TotPart=63, TrialsPerPart=36):
        self.path = path
        self.TotPart = TotPart
        self.TrialsPerPart = TrialsPerPart
        self.df = pd.read_csv(path)

    def unpack_HM_Matrix(self, trial, show_Mat=True):
        arr = np.array(eval(self.df.iloc[:, 5][trial]))

        t_arr = np.array(arr[0][0]).astype(np.float64)
        x_arr = np.array(arr[0][1])
        y_arr = np.array(arr[0][2])

        for i in range(1, arr.shape[0]):
            t_arr = np.append(t_arr, arr[i][0]).astype(np.float64)
            x_arr = np.append(x_arr, arr[i][1])
            y_arr = np.append(y_arr, arr[i][2])

        txy = np.column_stack((t_arr, x_arr, y_arr))

        if show_Mat:
            print("Matrix unpacked: \n", txy)
            return txy
        else:
            return txy

    # Number of participants to construct HM matrix
    def get_HM_Matrices(self, Npart=1, verbose=True, log_HM_list=False):
        # Confirm for correct dimension
        if ((self.TotPart*self.TrialsPerPart) < self.df.shape[0]):
            print(self.TotPart*self.TrialsPerPart, "\n", self.df.shape[0])
            raise ValueError(
                "Unmatch between df dimension and trial/participants quantity")
        p = 0
        mean_speed = []
        std_speed = []
        if log_HM_list:
            HM_list = []

        while p != Npart:
            for trial in range(self.TrialsPerPart):
                print("Trial #{}".format(trial), "|",
                      "Participant #{}".format(p))
                if ((trial % self.TrialsPerPart-1 == 0) and (trial != 0)):
                    p += 1

                mat_txy = self.unpack_HM_Matrix(trial, show_Mat=False)
                HM_mat = np.column_stack((mat_txy, self.adj_speed_ang_sec(
                    self.get_ang_speed(mat_txy), mat_txy.shape[0]/30)))

                # Flag in case of you need to prevent using too much memory while saving matrices
                if log_HM_list:
                    HM_list.append(HM_mat)

                # Removing NaN elements
                x = HM_mat[:, 3]
                x = x[~np.isnan(x)]

                # Adjusting time unit
                mean_speed.append(np.mean(x))
                std_speed.append(np.std(x))

                if verbose:
                    print("MATRIZ CALCULADA PARA O TRIAL {} \n".format(trial))
                    print(HM_mat)
                    print("DIMENSAO DA MATRIZ \n", HM_mat.shape)

                    print("coluna para velocidade  \n", x[~np.isnan(x)])
                    print("Mean speed: \n", self.adj_speed_ang_sec(
                        np.array(mean_speed), HM_mat.shape[0]/30.))
                    print("Std speed: \n", std_speed)
                    print("Sampling rate (1/s): \n", HM_mat.shape[0]/30)

        # When leaving the participant loop, save the mean values for the following statistical analysis
        if Npart == self.TotPart:  # In case all means were calculated
            self.update_df(mean_speed, std_speed)

        if log_HM_list:
            return HM_list, mean_speed, std_speed
        else:
            return HM_mat, mean_speed, std_speed

    def update_df(self, mspd, dspd, save_df=True):
        new_df = self.df.copy()
        new_df['Mean_speed'] = mspd
        new_df['Std_speed'] = dspd
        if save_df:
            new_df.to_csv(self.path+"HM.csv", index_label=False)
            print("### Matix successfully updated ###\n {}".format(
                self.path+"HM.csv"))

    def get_ang_speed(self, txy):
        t0 = txy[0][0].astype(np.float64)
        x0 = txy[0][1]
        y0 = txy[0][2]
        x1 = txy[1][1]
        y1 = txy[1][2]
        t1 = txy[1][0].astype(np.float64)
        pos0 = self.eulerian_to_cartesian(x0*2*np.pi, y0*np.pi)
        pos1 = self.eulerian_to_cartesian(x1*2*np.pi, y1*np.pi)
        d = self.compute_orthodromic_displacement(pos0, pos1)
        speed = np.nan
        speed = np.append(speed, np.array(d/(t1-t0)))

        for txy_el in txy[2:]:
            pos0 = pos1
            pos1 = self.eulerian_to_cartesian(
                txy_el[1]*2*np.pi, txy_el[2]*np.pi)
            d = self.compute_orthodromic_displacement(pos0, pos1)

            t0 = t1
            t1 = txy_el[0].astype(np.float64)

            speed = np.append(speed, np.array(d/(t1-t0)))
        return speed

    def eulerian_to_cartesian(self, theta, phi, r=1):
        """
            Description: The longitude ranges from 0, to 2*pi
            The latitude ranges from 0 to pi, origin of equirectangular in the top-left corner
            Return: the values (x, y, z) of a unit sphere with center in (0, 0, 0)
        """
        x = r*np.cos(theta)*np.sin(phi)
        y = r*np.sin(theta)*np.sin(phi)
        z = r*np.cos(phi)
        return np.array([x, y, z])

    def compute_orthodromic_displacement(self, ini_position, pos_position):
        """
            Description: Compute the orthodromic distance between two points in 3d coordinates
            Return: Distance between two points in the sphreical shell
        """
        norm_a = np.sqrt(np.square(
            ini_position[0]) + np.square(ini_position[1]) + np.square(ini_position[2]))
        norm_b = np.sqrt(np.square(
            pos_position[0]) + np.square(pos_position[1]) + np.square(pos_position[2]))
        x_ini = ini_position[0] / norm_a
        y_ini = ini_position[1] / norm_a
        z_ini = ini_position[2] / norm_a
        x_pos = pos_position[0] / norm_b
        y_pos = pos_position[1] / norm_b
        z_pos = pos_position[2] / norm_b
        great_circle_distance = np.arccos(np.maximum(np.minimum(
            x_ini * x_pos + y_ini * y_pos + z_ini * z_pos, 1.0), -1.0))
        return great_circle_distance

    def adj_speed_ang_sec(self, dang, sr):
        """
            Description: Transformation to second time units considering fixed 60 fps videos
            dang (np array): angular displacement (great_circle_distance) measured from two subsequent samples
            sr (float): samping rate of that trial
        """
        return dang*(2/sr)

    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx]


class FrameMetric(HM_Matrix):
    def __init__(self, inputs, out_path):
        """
        Description: Compute the align metric from an HM matrix
        input: unpacked head motion dataframe
        """
        super().__init__(inputs)
        self.M = HM_Matrix(inputs)
        self.src = np.array(self.df['src'])
        self.Edit_type = np.array(self.df['group'])
        self.inputs = inputs
        self.out_path = out_path
        if (type(self.inputs) != str):
            raise ValueError(
                'Original source may have a input of the type str containing the csv file path')

    def get_AlignMetric_df(self, Npart=1, save_df=False, verbose=True):
        # Description: Method to combine all methods in order to
        # build a dataframe with {participant,trial,Metric} structure for the analysis
        p = 1
        dist_arr = np.zeros(Npart*self.TrialsPerPart)
        while p != Npart+1:
            for trial in range(self.TrialsPerPart):
                # print("Trial #{}".format(trial), "|","Participant #{}".format(p) )

                mat_txy = self.unpack_HM_Matrix(trial, show_Mat=False)
                # timestamp of the metric computation (the end of the edit is at 16 sec)
                t = self.getRefTime(mat_txy, t_comp=16)
                pos_comp = self.getPosTime(t, mat_txy)
                pos_target = self.get_target_pos(trial)
                dist_target = self.getDistTarget(pos_comp, pos_target)
                # print("POS1", pos_comp, "POS Target", pos_target, "Distance", dist_target)
                # Save the computed trial value in the trial vector
                # Adjust limits for element-wise assingment
                if (p == 1):
                    dist_arr[trial] = dist_target
                else:
                    dist_arr[self.TrialsPerPart*(p-1)+trial] = dist_target
            p += 1

        # Build subject and trial columns no N
        if (Npart == 1):
            part_col = np.arange(1, self.TrialsPerPart+1)
        else:
            part_col = np.repeat(np.arange(1, Npart+1), self.TrialsPerPart)

        videos = self.src[:self.TrialsPerPart*Npart]
        Edit_type = self.Edit_type[:self.TrialsPerPart*Npart]
        trial_col = np.concatenate([np.arange(1, self.TrialsPerPart+1)]*Npart)

        if verbose:
            print("PARTICIPANTS", part_col, "\n Shape", part_col.shape)
            print("TRIAL", trial_col, "\n Shape", trial_col.shape)
            print("VIDEO", videos, "\n Shape", videos.shape)
            print("Edit type", videos, "\n Shape", Edit_type.shape)
            print("RESULT METRIC", dist_arr, "\n Shape", dist_arr.shape)
            print(Npart)

        # Stack all columns to build the final metric matrix
        distanceMetricCSV = np.column_stack(
            (part_col, trial_col, videos, Edit_type, dist_arr))

        if verbose:
            print("Final Matrix", distanceMetricCSV,
                  "\n Shape", distanceMetricCSV.shape)

        if (save_df):
            self.save_df(distanceMetricCSV)
        else:
            print(distanceMetricCSV)

    def get_timearr(self):
        return self.M.unpack_HM_Matrix(10)[:, 0]

    def getRefTime(self, txy, t_comp):
        """
        Description: return the best timestamp to compute metric
        """
        timearr = txy[:, 0]
        return self.find_nearest(timearr, t_comp)

    def getPosTime(self, t, txy):
        """
        Description: Return the csv line positions in a specific sample time
        """
        txy_end = txy[txy[:, 0] == t, :][0]
        return np.array((txy_end[1], txy_end[2]))

    def getDistTarget(self, pos, target):
        """
        Description: Computes the distance between two point on the unit sphere.
        Returns result in the units of provided radius.
        lats and lons are in degrees.

        pos: nparray containing center position in [0,1] screen position of the user viewport (normalized from equiretangular frame)
        target: np array containing center position in [0,1] screen position of the target viewport (normalized from equiretangular frame)
        """

        # Rescale spherical coordinates from [0,1] to spherical limits in radians
        x_target, y_target = self.rescaleNorm2Rad(target[0], target[1])
        x, y = self.rescaleNorm2Rad(pos[0], pos[1])

        Head_pos = self.eulerian_to_cartesian(x, y)
        Target_pos = self.eulerian_to_cartesian(
            x_target*2*np.pi, y_target*np.pi)
        return self.compute_orthodromic_displacement(Head_pos, Target_pos)

    def save_df(self, df_arr):
        # Receive an np array with all data and save it to a csv file
        pd.DataFrame(df_arr).to_csv(self.out_path, index_label=False)
        print("Successfully saved align performance CSV")
        print("Saved at: \n {}".format(self.out_path))

    def find_nearest(self, array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx]

    def rad2deg(self, rad):
        return (180 * rad)/np.pi

    def rescaleNorm2Rad(self, phiN, thetaN):
        phi = phiN*(np.pi)
        theta = thetaN*(2*np.pi)
        return theta, phi

    def unpack_HM_editType(self, trial, show_Mat=True):
        arr = np.array(eval(self.df.iloc[:, 5][trial]))

        t_arr = np.array(arr[0][0]).astype(np.float64)
        x_arr = np.array(arr[0][1])
        y_arr = np.array(arr[0][2])

        for i in range(1, arr.shape[0]):
            t_arr = np.append(t_arr, arr[i][0]).astype(np.float64)
            x_arr = np.append(x_arr, arr[i][1])
            y_arr = np.append(y_arr, arr[i][2])

        txy = np.column_stack((t_arr, x_arr, y_arr))

        if show_Mat:
            print("Matrix unpacked: \n", txy)
            return txy
        else:
            return txy

    def get_target_pos(self, trial):
        video = self.src[trial]  # get the src video of each trial
        # Dict containing the ROI positions for each video
        target_pos = {1: np.array([0.5, 0.5]),
                      2: np.array([0.5, 0.5]),
                      3: np.array([0.5, 0.5]),
                      4: np.array([0.5, 0.5]),
                      5: np.array([0.5, 0.5]),
                      6: np.array([0.5, 0.5])}
        return target_pos[video]
