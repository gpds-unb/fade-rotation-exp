
from HM_class import FrameMetric
import os

if __name__ == '__main__':

    res_path = os.getcwd()
    path = res_path+r'\data\raw_HM_19users.csv'
    out_path = res_path+r'\align_metric_19users.csv'

    f = FrameMetric(path, out_path)
    f.get_AlignMetric_df(19, save_df=True, verbose=True)
