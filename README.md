# User Quality of Experience (QoE) Enhancement with Alignment Edits

Welcome to the repository for the research project on optimizing User Quality of Experience (QoE) for 360° videos using alignment edits. This repository contains a DEMO of the code and samples used in our study, which was recently accepted in the IEEE Access Journal. The paper will be publicly available in the coming month. Our goal is to promote collaboration and share the scientific work we have conducted with the general public.

## Abstract

Optimizing user quality of experience (QoE) for 360° videos faces two major roadblocks: inaccurate viewport prediction and viewers missing the plot of a story. To tackle these issues simultaneously, alignment edits have emerged as a promising solution. Despite their potential benefits, there is limited knowledge about the actual impacts of alignment edits on user experience (UX). Therefore, we conducted subjective experiments following ITU-T P.919 methodology to explore their effects on QoE. Our proposed gradual alignment technique achieved a level of comfort and presence comparable to that of instant edits. These findings shed light on the positive effects of alignment edits on user experience and firmly establish the viability of the proposed gradual alignment technique to enhance QoE during video consumption.

## Authors

- [Lucas S. Althoff (Corresponding Author)](mailto:190051612@aluno.unb.br): Department of Computer Science, University of Brasília, Brasília, DF 70297400 Brazil
- [Mylene C. Q. Farias](mailto:mylene@ieee.org): Department of Computer Science, Texas State University, TX 7866 USA
- [Marcelo M. Carvalho](mailto:mmcarvalho@txstate.edu): Ingram School of Engineering, Texas State University, TX 7866 USA
- [Alessandro Rodrigues Silva](mailto:mestre.alessandros@gmail.com): University of Brasília, DF, Brazil

## Repository Contents

This repository contains code and samples related to our research. You will find the following:

### Code

In the "code" folder, we have:

1. [HM\_class.py](code/HM_class.py): This file contains the code for preparing and processing the head tracking data, following the step-by-step procedure described in Section V B. Specifically, the function `eulerian_to_cartesian()` is implemented there.

2. [main.py](code/main.py): This file contains the code for processing the compiled opinion scores and head tracking data. It implements the equations 6, 7, 8, 9, and 10.

3. [Statistics_scores.R](code/Statistics_scores.R): This file contains the code for processing the opinion scores data and executing hypotheses H1, H2, and H3. It implements the equations 1 and 2 used in the data analysis.

### Data

In the "data" folder, we have:

1. [raw_subject_19users.csv](data/raw_subject_19users.csv): This file contains part of the data extracted from the database, for testing purposes.

2. [HM_19users.csv](data/HM_19users.csv): This file contains part of the head tracking data extracted from the database, for testing purposes.

3. [align_metric_19users.csv](data/align_metric_19users.csv): This file contains the classified data in terms of alignment state.

4. [extract.sql](data/extract.sql): This file contains the code used for extracting the necessary data from the database.

### Usage

To reproduce the analysis presented in our paper, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the respective directory (R Code or Python Code) for the analysis you want to reproduce.
3. Install the required packages (mentioned above) if you haven't already.
4. Run the scripts following the instructions provided in the respective directories.

### Citation

If you find our work useful in your research, please consider citing our paper (once it is publicly available).

### Acknowledgment

The authors would like to thank Safaa Azzakhnini, André Henrique Costa, Henrique D. Garcia, Dario D. R. Morais, and Myllena Prado for providing inestimable insights and thoughts about this manuscript and/or about the experiment methodology. Their generosity and support have been invaluable in advancing the quality and scope of our work. We thank Filipe Gontijo, Henrique Siqueira, and Ana Arruda from XRBR and CaixoteVR studio for allowing us to use their exclusive content in our research.

This work was partially supported by FAPESP (Fundação de Amparo à Pesquisa do Estado de São Paulo), Grant 18/23086-1, by the Coordination for the Improvement of Higher Education Personnel – Brazil (CAPES) – Financing Code 001.
